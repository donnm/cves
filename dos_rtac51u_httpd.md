# dos_rtac51u_httpd

Denial of service (DoS) vulnerability in httpd in ASUS RT-AC51U wireless router firmware version up to and including 3.0.0.4.380.8591 allows attackers on the LAN to deny access to the administrative web UI.

Affects firwmare versions up to and including ASUS RT-AC51U 3.0.0.4.380.8591

Assigned CVE-2023-31889

## Prerequisites

Attacker must be connected to the LAN of the wireless router.

## Steps to reproduce

1. Open a TCP connection to ```httpd```:
  ```
  echo "GET / HTTP/1.1" | nc 192.168.1.1 80
  ```
2. Since the accept() loop of ```httpd``` is single-threaded, new connection attempts to the administrative web service will hang while the above netcat command active.

## Timeline

- 2022-04-07 Discovered
- 2022-05-18 Notified Asus
- 2022-05-23 Acknowledged, WONTFIX due to device end-of-life
- 2023-03-09 Report made public

## Credit

Ida Heggen Trosdahl, Jørgen Selsøyvold, Donn Morrison

Pentest bachelor thesis IDI/NTNU spring 2022
