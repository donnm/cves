# iv_glossary

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28262

The content of XLS files uploaded to the **Glossary** is not properly validated which leads to a denial of service (terms cannot be deleted) when terms contain invalid or unsupported characters.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Generate a file containing random stream of bytes:
```
head -c $(( 10*1024 )) > junk.xls
```
  2. Import the file under **Course Tools > Glossary** using the **Upload/Download > Upload Glossary** function
  3. When the file has been uploaded, junk characters will be visible
  4. Attempting to delete entries will fail

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/iv_glossary.mp4)

## Timeline

- 2020-10-30 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
