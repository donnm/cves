# CVEs

## Blackboard Learn

The following vulnerabilities affect Blackboard Learn version 3800.19.0-rel.54+54663c8.

### CVE-2020-28266 lfi_contentarea_package_import

A malicious path traversal in an exported **Content Area** XML package allows arbitrary server files to be read when the XML package is imported using the **Course Package Import** functionality. [Details](https://gitlab.com/donnm/cves/-/blob/master/lfi_contentarea_package_import.md)

### CVE-2020-28264 lfi_contacts_package_import

A malicious path traversal in an exported **Contacts** XML package allows arbitrary server files to be read when the XML package is imported using the **Course Package Import** functionality. This vulnerability is distinct from **lfi_contentarea_package_import** with respect to the XML tags used to host the path traversal. [Details](https://gitlab.com/donnm/cves/-/blob/master/lfi_contacts_package_import.md)

### CVE-2020-28265 xxe_survey

An XXE (XML external entity) vulnerability in an XML exported **Tests, Surveys, Pools** package allows for arbitrary server files to be read when imported using the corresponding import **Tests, Surveys, Pools** functionality. [Details](https://gitlab.com/donnm/cves/-/blob/master/xxe_survey.md)

### CVE-2020-28265 xxe_blind_http_ftp

An XXE (XML external entity) vulnerability in an XML exported **Tests, Surveys, Pools** package allows for arbitrary server files to be read and arbitrary URLs to be accessed when imported using the corresponding import **Tests, Surveys, Pools** functionality. Note: this vulnerability is similar to the above, with the added danger that it can be used to probe internal network assets. [Details](https://gitlab.com/donnm/cves/-/blob/master/xxe_blind_http_ftp.md)

### CVE-2020-28260 id_sendEmailCxLog and id_logDetails

A path traversal vulnerability leads to information disclosure in **Import Package / View Logs** (specifically **sendEmailCxLog** and **logDetails** actions) allowing a malicious user to query existence, size (in KB), and read access permission of arbitrary server files. [Details](https://gitlab.com/donnm/cves/-/blob/master/id_viewlogs.md)

### CVE-2020-28259 xss_TinyMCE

**UPDATE** Vendor noted that this is a configuration setting, allowing high-privelege (e.g., Teacher role) users to insert Javascript.

Arbitrary Javascript can be inserted into the HTML source editor component of the **TinyMCE editor** leading to a cross-site scripting (XSS) vulnerability. A malicious user (Instructor permissions only?) could trick other users into providing authentication credentials by replacing the document.innerHTML. The **TinyMCE editor** is used on virtually all course pages. [Details](https://gitlab.com/donnm/cves/-/blob/master/xss_TinyMCE.md)

### CVE-2020-28263 xss_glossary_import

A cross-site scripting (XSS) vulnerability in the **Glossary** upload functionality allows arbitrary Javascript to be added to entries in the **Glossary**. [Details](https://gitlab.com/donnm/cves/-/blob/master/xss_glossary_import.md)

### CVE-2020-28261 xss_contentarea_package_import

A cross-site scripting (XSS) vulnerability in the **Course Package Import** functionality allows arbitrary Javascript to be added to labels, filenames, and other fields. [Details](https://gitlab.com/donnm/cves/-/blob/master/xss_contentarea_package_import.md)

### CVE-2020-28262 iv_glossary

The content of XLS files uploaded to the **Glossary** is not properly validated allowing random data to be uploaded which leads to a denial of service (terms cannot be deleted). [Details](https://gitlab.com/donnm/cves/-/blob/master/iv_glossary.md)

### CVE-2020-28372 xxe_package_import

An XXE (XML external entity) vulnerability in the Import Package functionality allows arbitrary URLs to be accessed by the server which could lead to server-side request forgery (SSRF). [Details](https://gitlab.com/donnm/cves/-/blob/master/xxe_package_import.md)

### CVE-2020-28639 lfi_test_question_hot_spot

A malicious path traversal in an exported **Test** XML package allows arbitrary server files to be read when the XML package is imported using the **Import Test** functionality. [Details](https://gitlab.com/donnm/cves/-/blob/master/lfi_test_question_hot_spot.md)

## Asus

### CVE-2023-29772 ASUS RT-AC51U: XSS in web UI view System Log leads to device takeover

Cross-site scripting (XSS) vulnerability in the System Log/General Log page of the administrator web UI in ASUS RT-AC51U wireless router firmware version up to and including 3.0.0.4.380.8591 allows remote attackers to inject arbitrary web script or HTML via a malicious network request to the miniupnpd service. [Details](https://gitlab.com/donnm/cves/-/blob/master/xss_rtac51u_syslog.md)
