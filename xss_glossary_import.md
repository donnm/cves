# xss_glossary_import

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28263

A cross-site scripting (XSS) vulnerability in the **Create Term** and **Upload Glossary** functionality allows arbitrary Javascript to be added to entries in the **Glossary**.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Navigate to **Course Tools > Glossary**
  2. Create a new term and definition
  3. Download the glossary
  4. Edit the glossary, inserting Javascript before (term) or after (definition) the TAB separator:
```
<img src=x onerror=alert(1)>
```
  5. Save and upload the file to the glossary
  6. An alert box will be displayed

## Alternatively

  1. Navigate to **Course Tools > Glossary**
  2. Create a new term and definition with the following in the **Definition** field:
```
<img src=x onerror=alert(1)>
```
  3. Save the term
  4. An alert box will be displayed

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/xss_glossary_import.mp4)

## Timeline

- 2020-10-30 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
