# lfi_contacts_package_import

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28264

A malicious path traversal in an exported **Contacts** XML package allows arbitrary server files to be read when the XML package is imported using the **Course Package Import** functionality. This vulnerability is distinct from **lfi_contentarea_package_import** with respect to the XML tags used to host the path traversal.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce (assumes no existing contacts in the course)

  1. Navigate to **Course Tools > Contacts**
  2. Create a new contact, fill in required details
  3. Select a profile image to upload (any image)
  4. Export a course package selecting only the **Contacts**
  5. Unzip the package
  6. Edit the file ```imsmanifest.xml``` and change ```href``` attribute of the ```file``` tag to the desired file using a sufficient number of path traversal levels:
```
<file href="../../../../../../../../../../etc/passwd"/>
```
  7. Edit the file ```res00002.dat``` and change ```value``` attribute of the ```IMAGE``` tag to the **same** desired file using a sufficient number of path traversal levels:
```
<IMAGE value="../../../../../../../../../../etc/passwd"/>
```
  8. Zip the package and import using **Packages and Utilities > Import Package**, select **Contacts**
  9. Browse to the imported contact
  10. Right click on the broken image and select "View Image"

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/lfi_contacts_package_import.mp4)

## Timeline

- 2020-10-30 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
