# xss_contentarea_package_import

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28261

A cross-site scripting (XSS) vulnerability in the **Course Package Import** functionality allows arbitrary Javascript to be added to labels, filenames, and other fields.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Create a new Content Area
  2. Export a course package selecting only the created Content Area
  3. Unzip the package
  4. Search for the name of the Content Area in the unzipped files
  5. For each result, add some Javascript to the corresponding XML tag, e.g., in the file ```imsmanifest.xml```:
```
<title>XSS</title>
```
Becomes:
```
<title>&lt;img src=x onerror=alert(1)&gt;XSS</title>
```
  6. Zip the package and import using Packages and Utilities > Import Package, selecting the Content Areas checkbox
  7. An alert dialog box will appear when the import is complete and the page containing the name of the Content Area reloads

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/xss_contentarea_package_import.mp4)

## Timeline

- 2020-11-01 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
