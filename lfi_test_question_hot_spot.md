# lfi_test_question_hot_spot

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28639

A malicious path traversal in an exported **Test** XML package allows arbitrary server files to be read when the XML package is imported using the **Import Test** functionality.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Navigate to **Course Tools > Tests, Surveys, Pools > Tests**
  2. Create a new Test, fill in required details
  3. Create a new question of type **Hot Spot**
  4. Select an image to upload (```debian-security.png``` is used in this example and demo video below), click **Send**
  5. Drag a region in the image, click **Send**
  6. Navigate to **Course Tools > Tests, Surveys, Pools > Tests**
  7. Export the created test to your local machine
  8. Unzip the package
  9. Edit the file ```imsmanifest.xml``` and change ```href``` attribute of the ```file``` tag, replacing the original image path with the desired server file using a sufficient number of path traversal levels:
```
<file href="../../../../../../../../../../../../../../../etc/passwd"/>
```
  10. Edit the file ```res00001.dat``` and change ```uri``` attribute of the ```matapplication``` tag, replacing the original image path to the **same** desired server file using a sufficient number of path traversal levels:
```
<matapplication label="debian-security.png" apptype="application/octet-stream" uri="../../../../../../../../../../../../../../../etc/passwd" embedded="Inline"/>
```
  11. Zip the package and import it as a **Test** in **Course Tools > Tests, Surveys, Pools > Tests**
  12. Edit the imported test
  13. View the page source, search for ```passwd``` in the source and click the resulting link
  14. The exfiltrated file ```/etc/passwd``` from the server will be downloaded to your local machine

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/lfi_test_question_hot_spot.mp4)

## Timeline

- 2020-11-10 Discovered
- 2020-11-10 Notified vendor (Blackboard Inc.)
