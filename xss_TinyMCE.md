# xss_TinyMCE

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28259

Arbitrary Javascript can be inserted into the HTML source editor component of the **TinyMCE editor** leading to a cross-site scripting (XSS) vulnerability. A malicious user (Instructor permissions only?) could trick other users into providing authentication credentials by replacing the document.innerHTML. The **TinyMCE editor** is used on virtually all editable course pages.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Navigate to a page where elements can be created using the TinyMCE editor
  2. Open the editor
  3. Click the HTML source icon in the toolbar
  4. Insert Javascript
  5. Save the HTML source
  6. Save the element
  7. When the element is displayed, the Javascript will execute

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/xss_TinyMCE_phish.mp4)

## Timeline

- ?? Discovered (unsure if feature or bug, so did not initially report)
- 2020-11-06 Notified vendor (Blackboard Inc.)
