# lfi_contentarea_package_import

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28266

A malicious path traversal in an exported **Content Area** XML package allows arbitrary server files to be read when the XML package is imported using the **Course Package Import** functionality.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Create a **Content Area** element
  2. Upload a file to the folder
  3. Export a course package selecting only the created element
  4. Unzip the package
  5. Edit the file ```res00003.dat``` and change content of the ```<NAME>``` tag to the desired file using a sufficient number of path traversal levels:
```
<NAME>/../../../../../../../../../../../../../../../etc/passwd</NAME>
```
  6. Zip the package and import using **Packages and Utilities > Import Package**
  7. Open the folder
  8. Open the attached file (if you did not delete the original before re-import, there will be two entries - click on the second)

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/lfi_folder_package_import.mp4)

## Timeline

- 2020-11-01 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
- 2020-11-07 Resubmit on Bugcrowd due to non-reproducability
