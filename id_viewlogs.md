# id_sendEmailCxLog and id_logDetails

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28260

A path traversal vulnerability leads to information disclosure in **Import Package / View Logs** (specifically **sendEmailCxLog** and **logDetails** actions) allowing a malicious user to query existence, size (in KB), and read access permission of arbitrary server files.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Navigate to **Import Package / View Logs**
  2. If no logs are visible, export and import a package
  3. Click on a log file
  4. Modify the ```logFileName``` URL parameter to point to an arbitrary file using path traversal:
```
logFileName=../../../../../../../../../../../../../../../etc/passwd
```
  5. The size of the file will be visible under the displayed field "Log File Size"

Note: If the file does not exist, another information disclosure is displayed in the error page disclosing the absolute path to the log files on the server:

```
Error
Content
/usr/local/bbcontent/vi/BB5def77a38a2f7/courses/1/194_TDAT3020_A_2020_H_1/archive/../../../../../../../../../../../../../../../etc/passwd (No such file or directory)

For reference, the Error ID is 1c700205-0d9d-4af9-b375-94b5929d8ca0.

Thursday, November 5, 2020 5:04:53 PM CET
```

[Video demo (sendEmailCxLog)](https://gitlab.com/donnm/cves/-/raw/master/demos/id_emailLog.mp4)

[Video demo (logDetails)](https://gitlab.com/donnm/cves/-/raw/master/demos/id_logDetails.mp4)

## Timeline
- 2020-10-31 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
