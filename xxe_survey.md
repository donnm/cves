# xxe_survey

Affects Blackboard Learn 3800.19.0-rel.54+54663c8

Assigned CVE-2020-28265 (same CVE as [xxe_blind_ftp](xxe_blind_ftp.md))

An XXE (XML external entity) vulnerability in an XML exported **Tests, Surveys, Pools** package allows for arbitrary server files to be read when imported using the corresponding import **Tests, Surveys, Pools** functionality.

## Prerequisites

  - Instructor role in Blackboard Learn

## Steps to reproduce

  1. Navigate to **Course Tools > Tests, Surveys, Pools > Surveys**
  2. Build a new survey with a name and description and save it without adding any questions
  3. Export and save the survey locally by clicking on the dropdown arrow next to the survey name
  4. Unzip the package
  5. Edit the file ```res00001.dat``` and add the following after ```<?xml version="1.0" encoding="UTF-8"?>```:
```
<!DOCTYPE data [ <!ENTITY file SYSTEM "file:///etc/passwd"> ]>
```
  6. Insert ```&file;``` as content in the tag ```<mat_formattedtext type="HTML">``` that contains the description you entered when you created the survey:
```
<mat_formattedtext type="HTML">&lt;p&gt;&file;Sample description&lt;/p&gt;</mat_formattedtext>
```
  7. Zip the package and import it as a survey in the **Surveys**
  8. Edit the survey using the dropdown next to the survey name
  9. The file contents should appear in the description 

Caveats:
  1. Exfiltrated file must be ASCII
  2. Exfiltrated file must not contain < or >
  3. Exfiltrated file must be readable by user ```bbuser```

[Video demo](https://gitlab.com/donnm/cves/-/raw/master/demos/xxe_survey.mp4)

## Timeline

- 2020-10-27 Discovered
- 2020-11-06 Notified vendor (Blackboard Inc.)
- 2020-11-10 Vulerability reproduced (confirmed) by Bugcrowd
